//U ovom jednostavnom primeru cu prikazati upotrebu iteratora


//Recimo da imamo objekat "person1", i zelimo da prikazemo sve vrednosti 
//koje se nalaze u tom objektu, u primeru dole zelimo prikazati
// Ilija, Krstic, Programming, Simracing

let person1 = {
  name: "Ilija",
  lastName: "Krstic",
  hobbies: ["Programming", "Simracing", "F1"],
}

//Prva pomisao bi bila da prodjemo kroz objekat pomocu for...in
//Medjutim u slucaju hobbies, tu bi dobili ["Programming", "Simracing", "F1"]
//To nije ono sto zelimo, vec zelimo dobiti sve odvojeno

console.log("============= USING FOR...IN ==============")
for(const prop in person1){
  console.log(person1[prop])

  //Jedna od opcija bi bila for u for-u da uradimo
}


//Sledeca mogucnost koju imamo je da koristimo for...of
//Medjutim ako bi pokusali tek tako da prodjemo
//izbacilo bi gresku da objekat nije iterable

//U tom slucaju mi mozemo dodati nas custom iterator pomocu [Symbol.iterator]()

//Svaki iterator ima svoju funkciju "next()" koja vraca objekat {value, done: true/false}
//for...of ce raditi i uzimati "value" sve dok "done" nije "true"

console.log("============= USING ITERATORS ==============")
let person2 = {
  name: "Ilija",
  lastName: "Krstic",
  hobbies: ["Programming", "Simracing", "F1"],

  [Symbol.iterator](){ //Custom iterator
    let fields = Object.values(this) //Izvlacimo sve polja iz objekta
    //i dobicemo niz kao da smo prosli sa for...in
    
    index = 0; //Index bolja u objektu, fields[index]
    innerArrayIndex = 0; //Index u slucaju da je array polje
    return {
      next() { //Custom implementacija funkcije next()
        if(index >= fields.length) return { value: undefined, done: true} //U slucaju da je index === length return done:true

        if(Array.isArray(fields[index])) { // U slucaju da je polje array
          //U slucaju da je innerArrayIndex manji od length vrati vrednost, povecaj index, i vrati done:false
          if(innerArrayIndex < fields[index].length) return { value: fields[index][innerArrayIndex++], done: false } 
          else{
            //U slucaju da je innerArrayIndex === length restartuj taj index
            innerArrayIndex = 0
            //U slucaj da je naredni index veci od fields.length vrati done: true
            if(++index >= fields.length) return { value: undefined, done: true}

          }
        }

        return { value: fields[index++], done: false} //Vrati value i done:false
      }
    }
  }
}

//Zahvaljujuci custom iteratoru imamo mogucnost for...of
//kao i samo console.log(...person2)

for (const values of person2){
  console.log(values)
}

console.log(" ")

console.log(...person2)