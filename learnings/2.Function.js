/*
  ============================================================
  |                FIRST CLASS FUNCTION                      |
  ============================================================

  Za programski jezik kazemo da ima first class funkcije 
  ukoliko se one u tom jeziku tretiraju kao bilo koja druga 
  varijabla. Kao primer: 

  1. Funkcija moze biti prosledjenja kao parametar
  2. Moze biti vracena od strane druge funkcije
  3. Moze biti assigned kao vrednost varijable

*/

// ======================= PRIMER #1 =========================
// ---------- Prosledjivanje funkcije kao Argument -----------

function sayHello() {
  return 'Hello';
}

function greeting(helloMessage, name) {
  console.log(helloMessage() + ' ' + name);
}

// Prosledjujemo funkciju 'sayHello' kao parametar drugoj
// funkciji. Tom parametru ne dodajemo (),
// inace ce se ona pozvati

greeting(sayHello, 'Ilija');

/* 
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Funkcije koje prosledjujemo kao parametar se 
    nazivaju CALLBACK funckije
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

// ===========================================================
// ======================= PRIMER #2 =========================
// ------------------- Vracanje funkcije ---------------------

function sayHello2() {
  console.log('Hello Ilija #2');
}

function greeting2() {
  return sayHello2();
}

greeting2();

/* 
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Funkcije koje vracaju funkcije se nazivaju
    Higher-Order Function
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

// ===========================================================
// ======================= PRIMER #3 =========================
// -------------- Assign function to variable ----------------

const sayHello3 = function () {
  console.log('Hello Ilija #3');
};

sayHello3();

// ===========================================================

const sayHello4 = function (number) {
  return function () {
    console.log('Hello Ilija #' + number);
  };
};

/*
  U ovom slucaju dobijamo vrednost koju nam vraca funkcija 
  sayHello()
*/

let myFunc = sayHello4(4);

/*
  U ovom slucaju smo samo pretvorili sayHello4 u myFunc
  recimo da smo je preimenovali, ali ne i pozvali (Invoke)
  i zato ne vidimo 'Hello Ilija #5'
*/

let myFunc2 = sayHello4;

myFunc();
myFunc2(5); //Ne poziva se unutrasnja vrednost tj. return value

/*
  Ukoliko zelimo da pozovemo vrednost tj funkciju koju nam
  je vratila funkcija sayHello, mozemo da koristimo duple 
  zagrade 'double parentheses ()()', u tom slucaju direktno 
  pozivamo vracenu vrednost
*/
myFunc2(6)();

// ============= Immediately Executing Function ==============

/* 
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Funkcije koje pozivaju same sebe, tj izvrsavaju se odmah
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  */

(function () {
  console.log('Hello Ilija #7 (Immediately Executing Function)');
})();

// ===========================================================

// ========================= SCOPE ===========================

let pet = 'Cat (Outside function)';

function myPet() {
  let pet = 'Dog (Inside function)';
  console.log(pet);
}

myPet();
console.log(pet);

/* 
  Svaki put kada je funkcija pozvana, novi scope se kreira
  varijable koje su kreirane unutar funkcije mogu sluziti
  samo unutar funkcije i one nece ometati varijable
  koje su kreirane izvan funkcije
*/

/*
  Koriscenjem anonimnih funkcija i scopa je bolji nacin
  za koriscenje privatnih varijabla koje ce nestati kada
  se anonimna funkcija zavrsi
*/

// ===========================================================
// ======================== PRIMER ===========================

// Razlici prva dva broja dodati treci broj
// i prikazati resenje u konzoli
let num1 = 2;
let num2 = 3;
let num3 = 4;
let sum;

(function () {
  //Varijabla diff ce nestati cim se funkcija zavrsi
  let diff = num2 - num1; // diff = 1
  sum = diff + num3; // sum = 1 + 4
})();

console.log(sum, ' Sum #1');

/* 
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if,else, ili while ne kreiraju novi scope
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

// ======================= CLOSURES ==========================

/*
  Ukoliko se unutar funkcije kreira nova funkcija, unutrasnja
  funkcija ce imati pristup svim varijablama koje su kreirane
  u spoljasnoj funkciji, cak i ako je unutrasnja funkcija 
  return value
*/

function myPet2() {
  let pet = 'Cat (Closure)';

  function printMyPet() {
    console.log(pet);
  }

  printMyPet();
}

myPet2();

// Inner as return value

function myPet3() {
  let pet = 'Cat (Closure 2)';
  return function printMyPet() {
    console.log(pet);
  };
}

let myPet4 = myPet3();
myPet4();


// ====================== PARAMETERS =========================

function myPet5(pet){
  console.log(`My pet is ${pet}`)
}

/*
  U javiscript ne postoji provera parametara u vreme
  izvrsavanja, tako da ovde nece biti gresaka
*/

myPet5(); // My pet is undefined | ali nema error-a
myPet5("cat", "dog", "snake") // My pet is cat | nema error-a


// ====================== .call & .apply =========================

/*
  .call funkcija menja znacenje "this" keyword-a

  .apply radi istu stvar, jedina razlika u nacinu prosledjivanja parametra
  toj funckiji
*/

let obj1 = {
  name: "John Doe",
  greet: function greet(param1, param2){
    console.log(`Hello ${this.name} | ${param1}, ${param2}`)
  }
}

obj1.greet("param1", "param2") //Isprintace Hello John Doe

obj1.greet.call({name: "Ilija"}, "param3", "param4") //Isprintace Hello Ilija, parametri toj funkciji se prosledjuju odvojeni zarezima
obj1.greet.apply({name: "Ilija"}, ["param5", "param6"]) //Isprintace Hello Ilija, parametri toj funkciji se prosledjuju kao array

/*
  Ovom metodom takoreci mozemo "pozajmiti" neke funkcije, i promeniti parametre,
  tako da je ova funkcionalnost jako mocna
*/

//========================================================================

/*
  Zahvaljujuci skracenom obliku ne moramo pisati (return) jer se on podrazumeva
*/

let arrowFunctionReturn = () => true? "true": "false"

console.log(arrowFunctionReturn())


// ---------- NOTE ----------
/*
  Razlika izmedju ARGUMENATA I PARAMETRA

  kada definisemo funkciju, navodimo njene parametre tj.

  function hello(parametar1, parametar2)

  kada pozivamo funkciju, prosledjujemo argumente

  hello(argument1, argument2)

  parametri su potencijalni, a argumenti su stvari

  drugim recima su parametri cuvari mesta za argumente koje
  kasnije prosledjujemo
*/

// ---------- NOTE ----------

// ====================== Imena funkcije =========================

function sayHello5(){}

console.log("1. Function name is", sayHello5.name)

const sayHello6 = ()=>{}

console.log("2. Function name is", sayHello6.name)
// Iako je funckija anonimna tj nema imena
// Javascript ce pretpostaviti da je sayHello6 njeno ime

// Medjutim moguce je da funkcija stvarno nema ime

let arrayOfFunction = [];

arrayOfFunction[0] = ()=>{}

console.log("3. Function name is", arrayOfFunction[0].name) //u ovom slucaju funkcija zaista nema ime


// ====================== Default vrednost za parametar =========================

function sayHello7(name = "Ilija"){ 
  //Na ovaj nacin smo definisali da ukoliko ne posaljemo
  //argumen na mestu name, on ce biti "Ilija"
  console.log("Hello " + name)
}

sayHello7() //Hello Ilija
sayHello7("Sandra") //Hello Sandra

// ====================== Default vrednost za parametar =========================


//======================= Function HOISTING ===========================

sayHello8() // OK
function sayHello8(){console.log("test")}
sayHello8() // OK

// sayHello9() // ERRORCannot access 'sayHello9' before initialization
// let sayHello9 = () => console.log("test2")
// sayHello9()

//======================= Function HOISTING ===========================

//======================= Function DECLARATION ===========================

function square(x){
  return x*x
}

//======================= Function DECLARATION ===========================

//======================= Function EXPRESSION ===========================
const square = function(x){
  return x*x
}
//======================= Function EXPRESSION ===========================

//======================= ARROW Function ===========================
const square = (x) => {
  return x*x
}
//======================= ARROW Function ===========================
