const Emmiter = require("./Emmiter");

const emmiter = new Emmiter();

// Krairamo event 'test', i definisemo radnju/funkciju
// koja treba da se desi kada se taj event okine
emmiter.on("test", function(){ 
  console.log("On Test")
})

emmiter.on("test", function(){ 
  console.log("On Test #2")
})

emmiter.emit("test") //pozivamo funkciju emit i prosledjujemo 
// naziv eventa u ovom slucju "test"