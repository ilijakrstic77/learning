/*
 Cilj je kreirati funkciju koja ce moci da se pozove samo jednom,
 kao recimo kada kupac ode u svojoj korpi i klikne na "BUY",
 ukoliko bude kliknuo vise puta, potrebno da se samo jednom naplati
 a ne vise puta
*/

function purchase(name) { // Funkcija Hello koja treba samo jednom da se pozove
  console.log(`Client ${name} is purchased`)
}

function once(fn){ //Funckija preko koje cemo kreirati ovu funkcionalnost
  let called = false; //default vrednost je false, sto znaci da funkcija nikad nije pozvana
  //svaki put kada once bude bila povezana za nekom drugom funkcijom bice kreirana 
  //lokalna varijabla called sa vrednoscu false

  return (...args) => { //Funkcija once vraca funkciju koja prima neke parametre vise njih ili ni jedan
    if(!called) fn(...args) // ukoliko je called false, pozvace se funkcija koju smo prosledili
    //args je tip Array [ 'Ilija' ]
    called = true;
  }
}

purchaseOnce = once(purchase); //Ovde pozivamo once, prosledjujemo funkciju koju zelimo da okinemo jednom
//a to je funkcija purchase
//za sada purchase jos uvek nije pozvana, bice kada jednom budemo okinuli purchaseOnce...
//ovim smo mi purchase zapravo konvertovali u purchaseOnce varijablu koja je zapravo return 
//vrednost iz once...

purchaseOnce("Ilija")//ovde pozivamo purchaseOnce, tj return vrednost iz once, a u toj tome se nalazi
//deo u kome pozivamo funkciju koju smo bili prosledili na liniji 23 tj purchase
//i tek sada se poziva purchase i called prelazi u false

purchaseOnce("Ilija") //Ovde se zapravo nece okinuti purchase jer je iznad called prebaceno na false

