
//======================= ARRAY CHUNKING ==========================

//Recimo da imamo veliki array podataka koje trebamo da procesuiramo
let arr = [];
for (let i = 0; i < 20000; i++) {
  arr.push(i)
}
//=============

//Ukoliko koristimo klasicnu metodu prolaskom kroz array i pozivamo procesuiranje tih podataka
//Dovescemo do toga da cela aplikacija bude blokirana, sve dok se ne zavrsi ceo taj array
//U nasem dole primeru, ukoliko pozovemo "proccessArray(arr)", for ce proci 20k iteracija,
//A tek zatim ce biti prikazano "MIDDLE" u konzoli, "MIDDLE" predstavlja neku drugu funkciju
//Recimo drugi api poziv, ili na frontu klik dugmeta

//Medjutim ukoliko pozovemo "processArrayInChunk()", desava se drugacija situacija
//Ta funkcija ce prolaziti kroz array u chunk-ovima, u funkciji smo zakucali da je jedan
//chunk velicine 1000 iteracija, u nasem slucaju je okej jer je samo console.log
//Nakon sto for loop prodje 1000 iteracija, pozvace setTimeout(), koji ce pozvati istu funkciju
//osim sto ce start biti drugaciji tj povecan za CHUNK_SIZE, to ce ovu funkciju 
//staviti na kraju EVENT QUEUE, sto ce omoguciti da se izvrsi "MIDDLE" console log nakon 1000 iteracija
//a zatim ce array nastaviti kao i do sada, praviti chunkove od 1000
//U sustini setTimeout ce samo ostaviti malo prostora da se izvrse i druge stvari u aplikaciji a ne blokirati app

console.log("START");
// proccessArray(arr)
processArrayInChunk(arr, 0)
console.log("MIDDLE");


function processArrayInChunk(arr, start){
  let finished = false;
  const CHUNK_SIZE = 1000;
  for (let i = start; i < start + CHUNK_SIZE; i++) {
    console.log(`PROCESING DATA: ${i}`)  //Proccess funkcija 
    if(i === arr.length - 1) finished = true 
  }

  if(!finished)setTimeout(() => processArrayInChunk(arr, start + CHUNK_SIZE), 0)
}

function proccessArray(arr){
  for (let i = 0; i < arr.length; i++) {
    console.log(arr[i])
  }
}

//================================================================

 