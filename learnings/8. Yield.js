function* generatorFunction(i){
  yield i;
  yield i + 10;
  yield "TEST";
}

//yield stoping execution of function and return value,
//later you can continue executing function with .next()

//same as return exept function can be continued with .next()

const iterator = generatorFunction(10)

console.log(iterator.next()) // 10
console.log(iterator.next()) // 20
console.log(iterator.next()) // TEST
