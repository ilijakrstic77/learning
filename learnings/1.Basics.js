/*
  Javascript koristi najvise sintakse pozajmljenje iz Jave, C
  i C++, ali takodje je na sintaksu uticao i Awk, Perl i Python

  Javascript je case-sensitive i koristi Unicode.

  Tako da za ime varijable mozemo koristiti i imena kao
  Früh, 
*/

let Früh = "foo"

/*  
  Ali varijabla Früh nije ista kao früh cato sto je Javascript
  case-sensitive
*/

// ======================= STATEMENTS =========================

// U JavaScript jeziku instrukcije se nazivaju 'STATEMENTS'

// ======================= VARIABLES =========================

var foo = "foo" // classic variable

let bar = "bar" // block-soped, local variable

const foobar = "foobar" // block-scoped, read-only varijabla

/*
  Upotrebom keyword-a "var", kao primer var foo = "foo",
  moze se deklarisati i localna i globala varijabla
  u zavistosti od kontext-a

  Upotrebom keyword-a "let ili const", kao primer let ili const bar = "bar",
  kraira se varijabla koja je block-scoped, localna varijabla
*/

// =================== VARIABLE SCOPE ======================

/*
  Kada se varijabla kreira izvan funkcije, naziva se globalnom
  varijablom, ali kada se kreira unutar funkcije to je localna

  To je zato sto se u primeru globalne varijable ona nalazi 
  na nivou tog fajla i bice dostupna u svim delovima koji su 
  hijeraski ispod

  U primeru lokalne varijable, ta varijabla je dostupna
  samo na nivou te funkcije
*/

// PRIMER: 1

if(true){
  var x = "10"
}

console.log(x) // foo = 'bar'

// PRIMER: 2
if(true){
  let y = "20"
}

//console.log(y) // ReferenceError: y is not defined



// ================== VARIABLE HOISTING ====================

/*
  Jos jedna cudna stvar u vezi JavaScript jezika je to sto mozes
  da pozoves funkciju pre deklarisanja bez da dobijes error, s tim
  sto ce vrednost ipak biti undefined

  Ovaj koncept se naziva "hoisting" ili lakse receno "lifting" ili
  podizanje na vrhu funkcije, tako da cak i ako deklarises ili
  inicijalizujes ili referises na toj varijabli, idalje ces dobiti
  undefined
*/

// PRIMER: 1

console.log(z === undefined); //true
var z = 3;

// PRIMER: 2

var myVarX = "my var x";

(function(){
  console.log(myVarX); // undefined
  var myVarX = "local variable"
})()

// Kod koji je napisan gore ce se interpretirati na sledeci nacin

/*
PRIMER: 1

var x;  <---- Deklarisanje je "hoisted" ili podignuto na vrhu
console.log(x === undefined); // true
x = 3;

PRIMER: 2

var myVarX = 'my var x'; <---- Funkcije je deklarisana

(function() {
  var myVarX; <----Medjutim varijabla koju smo deklarisali 
                  ispod se penje na vrhu statement-a i time
                  gazimo globalnu varijablu i zato ce 
                  log ispod da ispise undefined
  console.log(myVarX); // undefined
  myVarX = 'local value';
})();

*/

// ================== CONTANTS ====================

const PI = 3.14;

//konstante u javiscript su read-only, tacnije njihova
//vrednost ne moze biti promenjena

//PI = 3; //Ovo bi izazvalo gresku

/*
  Medjutim polja unutar objekta koji je konstanta nisu zasticena
  tako da to nece izazvati gresku
*/

const person = { 
  name: "Ilija"
}

person.name = "Sandra";

console.log(person)

//Ali bi sledeci primer izazvao gresku

// person = {
//   name: "Ilija"
// }

//Na isti nacin i nizovi (Array) nisu zasticeni


// NaN

console.log(NaN === NaN, "NaN === NaN") //NaN je jedina vrednost koja nije jednaka sama sebi

