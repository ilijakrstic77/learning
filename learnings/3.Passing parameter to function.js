//Kreiranje object litterals 

let person = {
  firstName: "il",
  lastName: "Krstic",
  printFullName: function(){
    let fullName = `${this.firstName} ${this.lastName}`
    console.log(fullName)
  }
}


person.printFullName()

function changeName(person, newName) {
  person.firstName = newName

}

changeName(person, "Ilija")

person.printFullName()