/* 

HACKERRANK PROBLEM = https://www.hackerrank.com/challenges/magic-square-forming/problem

We define a magic square to be an n x n matrix of distinct positive integers from 1 to n(pow2) where the sum of any row, column, or diagonal of length n is always equal to the same number: the magic constant.
You will be given a 3x3 matrix s of integers in the inclusive range [1, 9]. We can convert any digit a to any other digit b in the range [1,9] at cost of [a-b]. Given s, convert it into a magic square at minimal cost. Print this cost on a new line.
Note: The resulting magic square must contain distinct integers in the inclusive range [1, 9].
*/

let input = [[4,5,8],[2,4,1],[1,9,7]]

function formingMagicSquare(input) {
  let minChanges = 99999
  posibleCombination.forEach(comb => {
    let changes = compare(input, comb)
    if(changes < minChanges) minChanges = changes;
  })

  return minChanges;
}

function compare(input, combination){
  let changes = 0;
  for (let y = 0; y < 3; y++) {
    for (let x = 0; x < 3; x++) {
      let first = input[y][x];
      let second = combination[y][x]
      changes += Math.abs(first - second);
    }
    
  }
  return changes
}

var posibleCombination = [
  [
    [8, 1, 6],
    [3, 5, 7],
    [4, 9, 2],
  ],
  [
    [6, 1, 8],
    [7, 5, 3],
    [2, 9, 4],
  ],
  [
    [4, 9, 2],
    [3, 5, 7],
    [8, 1, 6],
  ],
  [
    [2, 9, 4],
    [7, 5, 3],
    [6, 1, 8],
  ],
  [
    [8, 3, 4],
    [1, 5, 9],
    [6, 7, 2],
  ],
  [
    [4, 3, 8],
    [9, 5, 1],
    [2, 7, 6],
  ],
  [
    [6, 7, 2],
    [1, 5, 9],
    [8, 3, 4],
  ],
  [
    [2, 7, 6],
    [9, 5, 1],
    [4, 3, 8],
  ],
];


let res = formingMagicSquare(input);
console.log(res)

