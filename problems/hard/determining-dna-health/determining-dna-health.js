// HACKERRANK PROBLEM: https://www.hackerrank.com/challenges/determining-dna-health/problem

// Koriscen agoritam Aho-Corasick

// zbog velicine podataka za testiranje 50k-100k ovaj algoritam je bio super za ovakav zadatak

let dnadata = require('./dna-data2.json');
// let dnadata = require('./dna-data.json');


let allGenHp = [];

// U ovom for-u ispod samo spajamo svaki health sa genom, i upisujemo index gen-a
for (let i = 0; i < dnadata.genes.length; i++) {
  const gen = dnadata.genes[i];
  allGenHp.push({
    word: gen, // gen
    hp: {
      value: dnadata.health[i], //hp 
      index: i,
    },
  });
}

//root TRIE skrukture
let root = new Map();

// U ovoj funkciji kreiramo TRIE strukturu, parametri su w = rec, i = index, n = node
//Na pocetku je 'n' root koji smo kreirali iznad
//prolazeci skroz sve objekte koje smo kreirali pozivamo ovu funkciju, uzecemo u obzir
//da je sledeca rec "abc" i da je njen HP = 10
function createTree(w, i, n) {
  let nextNode;
  let word = w.word; //npr. "abc"
  let existing = n.get(word[i]); // Pretrazuje da li postoji node sa zadatim sufixom "a"

  if (!existing) { //Ukoliko ne postoji node sa zadatim sufixom
    nextNode = new Map(); // Kreira se novi
    n.set(word[i], nextNode); // key ce biti to slovo "a"
  } else nextNode = existing; // ukoliko postoji sledeci node koji gledamo ce biti taj koji smo pronasli

  if (word.length - 1 > i) createTree(w, i + 1, nextNode); //Ukoliko postoji sledece slovo u reci, nastavi pretragu
                                                           // U nasem slucaju je sledece slobo "b", 
                                                           // u sledecoj iteraciji ce biti slovo "c"
  else { //ukoliko smo na kraju reci tj kada budemo na slovu "c"
    nextNode.set('isWord', true); //postavljamo da je ovaj node zapravo i kraj 'neke' reci
                                  //tako da znamo ukoliko je kraj recenice i trenutni node je 
                                  //poslednji node neke reci onda je to pronadjeno, ovo je vazno u kasnijem search-u
                                  // tako da cemo imati a -> b -> c(kraj reci)

    let hp = nextNode.get('hp');  //uzimamo health array trenutnog noda-a
    if (!hp) { //provaravamo da li ima uopste health array
      nextNode.set('hp', [w.hp]); //ako nema, kreiramo novi
    } else {
      hp.push(w.hp); //ukoliko ima, pushujemo health trenutne reci, i sve se ovo desava na poslednjem nodu tj
                     // onom koji ima isWord = true
      nextNode.set('hp', hp);
    }
  }
}

allGenHp.forEach((w) => { // kreiranje trie strukture
  createTree(w, 0, root);
});


//trazicemo rec "abc" jer taj node structure smo kreirali gore
function searchWord(search, i, node) {
  if (search.length === i + 1 || !node.get(search[i])) // provaravamo da li smo na kraju reci 
    return node.get(search[i]) //ukoliko jesmo na kraju reci vracamo sledecu kombinaciju, ukoliko node sa trenutnim slovom postoji
      ? node.get(search[i]).get('hp') // i ukoliko ima svoj HP
        ? node.get(search[i]).get('hp') // vrati taj hp
        : [] //ukoliko nema HP, samo vrati prazan array
      : []; //ukoliko nema svoj node, vrati prazan array

  let nextNode = node.get(search[i]); //uzimamo node sa trenutnim slovom, slovom "a"

  let founded = searchWord(search, i + 1, nextNode);  //radimo rekurziju u pozivamo opet istu funkciju ali sa sledecim slovom, slovom "b"
                                                      //u sledecoj iteraciji cemo isto ovde doci ali sa slovom "c"

  //kada se rekurzija zavrsi u krene se "zatvarati" unazad tako cemo dobijati ovde vrednosti i vracati na dalje
  //tako da cemo dobiti HP od svih reci i paterana koji je nasao
  //recimo da imamo veci u node-u "car" "carg" "cargo", i mi pretrazujemo reci "cargo"
  //u ovom zadatku trebamo dobiti hp za svaku od ovih reci jer ce algoritam proci ovako
  //              c
  //               \
  //                a
  //                 \
  //                  r -> isWord (car) hp = 10, index = 2456 (note: ovo je index iz array-a health)
  //                   \
  //                    g -> isWord (carg) hp = 20, index = 245
  //                     \
  //                      o -> isWord (car0) hp = 30, index = 1984 

  //kada pretrazujemo rec cargo, mozemo naci patern car, carg, cargo, tako da uzimamo totalno hp 60


  if (nextNode.get('isWord')) founded = [...founded, ...nextNode.get('hp')]; // svaki novi hp iz primera iznad se prebacuje u founded i to se na kraju vraca sve
  return founded;
}

function calculateDNAHealth(first, last, d) {
  let tHp = 0;
  //primer DNA strukture "atabc"
  for (let i = 0; i < d.length; i++) { //prolazimo kroz svako slovo DNA strukture
    let wordForSearch = d.slice(i, d.length); //ali ovde uzimamo recenicu pocevsi od tog slova npr
                                              //atabc, tabc, abc, bc, c

    let founded = searchWord(wordForSearch, 0, root); // u prva dva pokusaja necemo naci nista na osnovu gore kreiranog noda,
                                                      // ali cemo u trecoj naci "abc" koji ima HP od 10
    for (let j = 0; j < founded.length; j++) {        // ovde dodajemo taj 10 u konacni koji cemo vratiti kao health zadate strukture
      const b = founded[j];                           // ovde sam na kraju odradio klasican for, jer mi je kesirao reduce i forEach iz nekog razloga
                                                      // bez obzira sto je hp u jednom testu bio [3,4,4,2,6] on je vracao vrednost 11 sve dok nisam uradio for
                                                      // a zatim posto sam znao, vratio sam reduce, i gle cuda radio je i vracao 19 :'D ali sam zadrzao ipak for
      if (b.index >= first && b.index <= last) tHp += b.value;
    }
  }
  return tHp;
}


function start() {
  let min = undefined;
  let max = undefined;
  dnadata.data.forEach((e, i) => {
    //prolazimo kroz sve DNA strukture i prosledjujemo prvi i poslednji index kao i samo strukturu
    let res = calculateDNAHealth(e.first, e.last, e.d);
    // console.log(e.first, e.last, e.d, res, "a")

    if (min === undefined) min = res;
    if (max === undefined) max = res;

    if (res < min) min = res;
    if (res > max) max = res;
  });

  console.log(min, max)
}
start();
