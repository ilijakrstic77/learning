//https://www.hackerrank.com/challenges/diagonal-difference/problem

function diagonalDifference(array) {
  // Write your code here
  let dif = 0;
  for (let i = 0; i < array.length; i++) {
    let num1 = array[i][i]
    let num2 = array[i][array.length - 1 - i]
    console.log()
    dif += num1 - num2;
    // console.log(num1, num2, dif)
  }
  return Math.abs(dif)
}

let arr = [[11,2,4],[4,5,6],[10,8,-12]]

diagonalDifference(arr)

//SECOND SOLUTION

let dif2 = Math.abs(arr.reduce((a, b, i) => a += b[i] - b[arr.length - 1 - i], 0))

console.log(dif2)