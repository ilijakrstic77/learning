let arr = new Array(6).fill(new Array(6).fill(1));
arr = fillArrayWithRandomNumber(arr);

arr = [
  [1,1,1,0,0,0],
  [0,1,0,0,0,0],
  [1,1,1,0,0,0],
  [0,0,2,4,4,0],
  [0,0,0,2,0,0],
  [0,0,1,2,4,0],
]


console.log(hourglassSum(arr))
function hourglassSum(arr) {
  let highest;
  for (let x = 1; x < arr.length - 1; x++) {
    const element = arr[x];
    for (let y = 1; y < element.length - 1; y++) {
      const number = element[y];
      let sum = [arr[x - 1][y - 1], arr[x - 1][y], arr[x - 1][y + 1], arr[x][y], arr[x+1][y-1], arr[x+1][y], arr[x+1][y+1]].reduce((a, b) => a + b, 0);
      if(sum > highest || highest == undefined) highest = sum;
    }
  }
  return highest
}

function getSumOrHourGlassPatern(arr, x, y) {
  return [arr[x - 1][y - 1], arr[x - 1][y], arr[x - 1][y + 1], arr[x][y], arr[x+1][y-1], arr[x+1][y], arr[x+1][y+1]].reduce((a, b) => a + b, 0);
}

function fillArrayWithRandomNumber(arr) {
  return arr.map((element) =>
    element.map((number) => randomIntFromInterval(0, 9))
  );
}

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}
