//HACKERRANK - https://www.hackerrank.com/challenges/insertionsort1/problem

let arr = [2, 3, 4, 5, 6, 7, 8, 9, 10, 1];
let num1 = arr[arr.length - 1];

for (let i = arr.length - 1; i >= 0; i--) {
  const num2 = arr[i - 1];
  if (num2 < num1 || i === 0) {
    arr[i] = num1;
    num1 = num2;
    console.log(arr.toString().replace(/,/g, ' '));
    break;
  } else {
    arr[i] = arr[i - 1 >= 0 ? i - 1 : i];
    console.log(arr.toString().replace(/,/g, ' '));
  }
}