let arr = [1,2,3,4,5]
let rot = 4

let rotLong = rotLeft(arr, rot)


function rotLeft(a, d) {

  for (let i = 0; i < d; i++) {
    a = rotation(a)
  }

  console.log(a)
}

function rotation(a){
  let holder;
  for (let i = 0; i < a.length; i++) {
    console.log(i)
    if(holder === undefined) holder = a[i];
    else{
      a[i-1] = a[i]
    }
  }

  a[a.length - 1] = holder

  return a
}

//============================= BETTER SOLUTION ===================================

function rotLeft(a, d) {

  let realRot = d % a.length

  if(realRot === 0) return a
 
  let final = [...a.slice(realRot, a.length), ...a.slice(0, realRot)];

  return final;
}

let rotShort = rotLeft(arr, rot)


//============================= BETTER SOLUTION SHORTER ===================================


const rotLeftShort = (a, d) => d % a.length === 0? a : [...a.slice(d % a.length, a.length), ...a.slice(0, d % a.length)]; 


let rotShorter = rotLeft(arr, rot)

console.log({
  rotLong,
  rotShort,
  rotShorter
})
