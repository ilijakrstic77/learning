/*
There is a large pile of socks that must be paired by color. 
Given an array of integers representing the color of each sock, 
determine how many pairs of socks with matching colors there are.

Example:

n = 7;
ar = [1,2,1,2,1,3,2];

There is one pair of color 1 and one of color 2. There are three odd socks left, one of each color. The number of pairs is 2.
*/

//SOLUTION 1.

let arr = [1,2,1,1,3,2];

function countPair1(arr){
  arr.sort((a, b) => a - b); //Upotreba custom sort-a jer bez toga moze biti primer [ 10, 2, 3, 50, 6 ]

  //Prvo sto radimo je sortiranje, dobicemo input [1,1,1,2,2,2,3]

  let pair = 0; // Broj parova na pocetku je 0

  for (let i = 0; i < arr.length; i+=2) { //prolazimo kroz sortiran array u parova tj povecavamo index za 2
    if(arr[i] === arr[i+1]) pair++ // Ako su prvi broj i drugi broj isti, dodajemo par
    else{ 
      i -= 1 // Ukoliko nisu, oduzimamo 1 index, i kasnije dodajemo 2 u sledecoj iteraciji
    }
  }

  return pair;
}

let solution1 = countPair1(arr)


//========================= BETTER SOLUTION =======================================


function countPair2(arr){
  let counted = [] //Array i kome stoje svi brojevi koji su vec proveravani
  let pair = 0;

  arr.forEach(number=>{
    if(!counted.includes(number)){ // Logika ispod radi samo ukoliko broj nije vec proveravan
      counted.push(number) //Registrujemo broj da je proveren
      let filtered = arr.filter(element => element === number); //Filtriramo samo taj jedan broj iz array-a
      pair += Math.floor(filtered.length / 2) //Zatim duzinu array filtriranih delimo sa 2, i uklanjamo sve posle zareza i to dodajemo u broj parova

      /*
        Ako je broj parova 2.5, sa Math.floor ce biti 2 i to dodajemo u pair
      */
    }
  })

  return pair
}
let solution2 = countPair2(arr)





// ================== SOLUTION 3 SHORT ================

let uniq = [...new Set(arr)]; console.log(uniq)
let solution3 = uniq.reduce((a, b) => a + Math.floor(arr.filter(c => b == c).length / 2), 0)

console.log({
  solution1,
  solution2,
  solution3
})