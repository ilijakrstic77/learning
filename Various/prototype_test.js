const util = require("util")
function test(){
  this.name = "test"
}

test.prototype.test = "aaaa"


function doSomething(){
  test.call(this)
}

util.inherits(doSomething, test)

doSomething.prototype.foo = "bar";

var doSomeInstancing = new doSomething();
doSomeInstancing.prop = "some value";
console.log("doSomeInstancing.test:      " + doSomeInstancing.test)
console.log("doSomeThing.test:           " + doSomething.test)
console.log("doSomeInstancing.prop:      " + doSomeInstancing.prop);
console.log("doSomeInstancing.foo:       " + doSomeInstancing.foo);
console.log("doSomething.prop:           " + doSomething.prop);
console.log("doSomething.foo:            " + doSomething.foo);
console.log("doSomething.prototype.prop: " + doSomething.prototype.prop);
console.log("doSomething.prototype.foo:  " + doSomething.prototype.foo);